namespace homework3
{
    partial class Form1
    {
        private System.ComponentModel.IContainer components = null;
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows 窗体设计器生成的代码
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.addFile_button = new System.Windows.Forms.Button();
            this.LineCountLabel = new System.Windows.Forms.Label();
            this.WordCountLabel = new System.Windows.Forms.Label();
            this.FormattedLineCountLabel = new System.Windows.Forms.Label();
            this.FormattedWordCountLabel = new System.Windows.Forms.Label();
            this.wordCountListBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Shrikhand", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(628, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "C# source file formatting and statistics program";
            this.addFile_button.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.addFile_button.Location = new System.Drawing.Point(109, 89);
            this.addFile_button.Name = "addFile_button";
            this.addFile_button.Size = new System.Drawing.Size(97, 40);
            this.addFile_button.TabIndex = 1;
            this.addFile_button.Text = "add file";
            this.addFile_button.UseVisualStyleBackColor = true;
            this.addFile_button.Click += new System.EventHandler(this.addFile_button_Click);
            this.LineCountLabel.AutoSize = true;
            this.LineCountLabel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LineCountLabel.Location = new System.Drawing.Point(67, 146);
            this.LineCountLabel.Name = "LineCountLabel";
            this.LineCountLabel.Size = new System.Drawing.Size(0, 18);
            this.LineCountLabel.TabIndex = 2;
            this.WordCountLabel.AutoSize = true;
            this.WordCountLabel.Location = new System.Drawing.Point(67, 177);
            this.WordCountLabel.Name = "WordCountLabel";
            this.WordCountLabel.Size = new System.Drawing.Size(0, 18);
            this.WordCountLabel.TabIndex = 3;
            this.FormattedLineCountLabel.AutoSize = true;
            this.FormattedLineCountLabel.Location = new System.Drawing.Point(67, 208);
            this.FormattedLineCountLabel.Name = "FormattedLineCountLabel";
            this.FormattedLineCountLabel.Size = new System.Drawing.Size(0, 18);
            this.FormattedLineCountLabel.TabIndex = 4;
            this.FormattedWordCountLabel.AutoSize = true;
            this.FormattedWordCountLabel.Location = new System.Drawing.Point(67, 237);
            this.FormattedWordCountLabel.Name = "FormattedWordCountLabel";
            this.FormattedWordCountLabel.Size = new System.Drawing.Size(0, 18);
            this.FormattedWordCountLabel.TabIndex = 5;
            this.wordCountListBox.FormattingEnabled = true;
            this.wordCountListBox.ItemHeight = 18;
            this.wordCountListBox.Location = new System.Drawing.Point(307, 89);
            this.wordCountListBox.Name = "wordCountListBox";
            this.wordCountListBox.Size = new System.Drawing.Size(293, 166);
            this.wordCountListBox.TabIndex = 6;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 317);
            this.Controls.Add(this.wordCountListBox);
            this.Controls.Add(this.FormattedWordCountLabel);
            this.Controls.Add(this.FormattedLineCountLabel);
            this.Controls.Add(this.WordCountLabel);
            this.Controls.Add(this.LineCountLabel);
            this.Controls.Add(this.addFile_button);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button addFile_button;
        private System.Windows.Forms.Label LineCountLabel;
        private System.Windows.Forms.Label WordCountLabel;
        private System.Windows.Forms.Label FormattedLineCountLabel;
        private System.Windows.Forms.Label FormattedWordCountLabel;
        private System.Windows.Forms.ListBox wordCountListBox;
    }
}
