using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace homework3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void addFile_button_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "请选择C#格式文件";
            openFileDialog.Filter = "C# files (*.cs)|*.cs|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;
                int originalLineCount = File.ReadLines(filePath).Count();
                int originalWordCount = GetWordCount(filePath);
                LineCountLabel.Text = $"原始行数：{originalLineCount}";
                WordCountLabel.Text = $"原始单词数：{originalWordCount}";
                string formattedContent = FormatSourceFile(filePath);
                File.WriteAllText(filePath, formattedContent);
                int formattedLineCount = File.ReadLines(filePath).Count();
                int formattedWordCount = GetWordCount(filePath);
                FormattedLineCountLabel.Text = $"格式化后行数：{formattedLineCount}";
                FormattedWordCountLabel.Text = $"格式化后单词数：{formattedWordCount}";
                var wordCountDict = GetWordCountDictionary(filePath);
                wordCountListBox.Items.Clear();
                foreach (var wordCount in wordCountDict)
                {
                    wordCountListBox.Items.Add($"{wordCount.Key}: {wordCount.Value}");
                }
            }
        }
        private int GetWordCount(string filePath)
        {
            string content = File.ReadAllText(filePath);
            string[] words = Regex.Split(content, @"\W+");
            int wordCount = words.Length;
            return wordCount;
        }
        private string FormatSourceFile(string filePath)
        {
            string content = File.ReadAllText(filePath);
            string formattedContent = Regex.Replace(content, @"^\s*//.*$", "", RegexOptions.Multiline);
            formattedContent = Regex.Replace(formattedContent, @"^\s*$\n|\r", "", RegexOptions.Multiline);
            return formattedContent;
        }
        private Dictionary<string, int> GetWordCountDictionary(string filePath)
        {
            string content = File.ReadAllText(filePath);
            string[] words = Regex.Split(content, @"\W+");
            var wordCountDict = words.GroupBy(w => w)
                                     .ToDictionary(g => g.Key, g => g.Count());
            return wordCountDict;
        }
    }
    }
