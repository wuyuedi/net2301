﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace StudentManagementSystem
{
    public partial class Form1 : Form
    {
        private List<School> schools;
        private SQLiteConnection connection;
        private Log log;

        public Form1()
        {
            InitializeComponent();
            schools = new List<School>();
            connection = new SQLiteConnection("Data Source=student.db;Version=3;");
            connection.Open();
            log = new Log(connection); // 初始化 log
            InitializeSchoolsFromDatabase();
        }

        // 初始化学校列表
        private void InitializeSchoolsFromDatabase()
        {
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "CREATE TABLE IF NOT EXISTS Schools (Id INTEGER PRIMARY KEY, Name TEXT)";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "CREATE TABLE IF NOT EXISTS Classes (Id INTEGER PRIMARY KEY, Name TEXT, SchoolId INTEGER)";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "CREATE TABLE IF NOT EXISTS Students (Id INTEGER PRIMARY KEY, Name TEXT, ClassId INTEGER)";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "SELECT * FROM Schools";
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = Convert.ToInt32(reader["Id"]);
                        string name = reader["Name"].ToString();
                        School school = new School(id, name);
                        schools.Add(school);
                        lbSchools.Items.Add(school);
                    }
                }
            }
        }

        private void btnCreateSchool_Click(object sender, EventArgs e)
        {
            string schoolName = txtSchoolName.Text;
            School newSchool = new School(schoolName);
            newSchool.SaveToDatabase(connection);
            schools.Add(newSchool);
            lbSchools.Items.Add(newSchool);
        }

        private void btnCreateClass_Click(object sender, EventArgs e)
        {
            if (lbSchools.SelectedItem != null)
            {
                School selectedSchool = (School)lbSchools.SelectedItem;
                string className = txtClassName.Text;
                Class newClass = new Class(className);
                newClass.SaveToDatabase(connection);
                selectedSchool.AddClass(newClass);
                lbClasses.Items.Add(newClass);
            }
            else
            {
                MessageBox.Show("请选择一个学校来创建班级。");
            }
        }

        private void btnCreateStudent_Click(object sender, EventArgs e)
        {
            if (lbClasses.SelectedItem != null)
            {
                Class selectedClass = (Class)lbClasses.SelectedItem;
                string studentName = txtStudentName.Text;
                string studentGender = txtStudentGender.Text;
                int studentAge = int.Parse(txtStudentAge.Text);
                string studentDepartment = txtStudentDepartment.Text;
                int classId = selectedClass.Id; 

                Student newStudent = new Student(studentName, studentGender, studentAge, studentDepartment, classId);
                newStudent.SaveToDatabase(connection);
                selectedClass.AddStudent(newStudent);
                lbStudents.Items.Add(newStudent);
            }
            else
            {
                MessageBox.Show("请选择一个班级来创建学生。");
            }
        }

        private void btnDeleteSchool_Click(object sender, EventArgs e)
        {
            if (lbSchools.SelectedItem != null)
            {
                School selectedSchool = (School)lbSchools.SelectedItem;
                selectedSchool.DeleteFromDatabase(connection);
                schools.Remove(selectedSchool);
                lbSchools.Items.Remove(selectedSchool);
                lbClasses.Items.Clear();
                lbStudents.Items.Clear();
            }
        }

        private void btnDeleteClass_Click(object sender, EventArgs e)
        {
            if (lbClasses.SelectedItem != null)
            {
                Class selectedClass = (Class)lbClasses.SelectedItem;
                selectedClass.DeleteFromDatabase(connection);
                lbClasses.Items.Remove(selectedClass);
                lbStudents.Items.Clear();
            }
        }

        private void btnDeleteStudent_Click(object sender, EventArgs e)
        {
            if (lbStudents.SelectedItem != null)
            {
                Student selectedStudent = (Student)lbStudents.SelectedItem;
                selectedStudent.DeleteFromDatabase(connection);
                lbStudents.Items.Remove(selectedStudent);
            }
        }

        private void btnUpdateSchool_Click(object sender, EventArgs e)
        {
            if (lbSchools.SelectedItem != null)
            {
                School selectedSchool = (School)lbSchools.SelectedItem;
                string newName = txtSchoolName.Text;
                selectedSchool.Name = newName;
                selectedSchool.UpdateInDatabase(connection);
                lbSchools.Items[lbSchools.SelectedIndex] = selectedSchool;
            }
        }

        private void btnUpdateClass_Click(object sender, EventArgs e)
        {
            if (lbClasses.SelectedItem != null)
            {
                Class selectedClass = (Class)lbClasses.SelectedItem;
                string newName = txtClassName.Text;
                selectedClass.Name = newName;
                selectedClass.UpdateInDatabase(connection);
                lbClasses.Items[lbClasses.SelectedIndex] = selectedClass;
            }
        }

        private void btnUpdateStudent_Click(object sender, EventArgs e)
        {
            if (lbStudents.SelectedItem != null)
            {
                Student selectedStudent = (Student)lbStudents.SelectedItem;
                string newName = txtStudentName.Text;
                selectedStudent.Name = newName;
                selectedStudent.UpdateInDatabase(connection);
                lbStudents.Items[lbStudents.SelectedIndex] = selectedStudent;
            }
        }

        private void btnSearchSchool_Click(object sender, EventArgs e)
        {
            string keyword = txtSearchSchool.Text;
            lbSchools.Items.Clear();
            foreach (School school in schools)
            {
                if (school.Name.Contains(keyword))
                {
                    lbSchools.Items.Add(school);
                }
            }
        }

        private void btnSearchClass_Click(object sender, EventArgs e)
        {
            string keyword = txtSearchClass.Text;
            lbClasses.Items.Clear();
            if (lbSchools.SelectedItem != null)
            {
                School selectedSchool = (School)lbSchools.SelectedItem;
                foreach (Class classObj in selectedSchool.Classes)
                {
                    if (classObj.Name.Contains(keyword))
                    {
                        lbClasses.Items.Add(classObj);
                    }
                }
            }
        }

        private void btnSearchStudent_Click(object sender, EventArgs e)
        {
            string keyword = txtSearchStudent.Text;
            lbStudents.Items.Clear();
            if (lbClasses.SelectedItem != null)
            {
                Class selectedClass = (Class)lbClasses.SelectedItem;
                foreach (Student student in selectedClass.Students)
                {
                    if (student.Name.Contains(keyword))
                    {
                        lbStudents.Items.Add(student);
                    }
                }
            }
        }

        private void lbSchools_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbClasses.Items.Clear();
            lbStudents.Items.Clear();

            if (lbSchools.SelectedItem != null)
            {
                School selectedSchool = (School)lbSchools.SelectedItem;
                selectedSchool.LoadClassesFromDatabase(connection);
                foreach (Class classObj in selectedSchool.Classes)
                {
                    lbClasses.Items.Add(classObj);
                }
            }
        }

        private void lbClasses_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbStudents.Items.Clear();

            if (lbClasses.SelectedItem != null)
            {
                Class selectedClass = (Class)lbClasses.SelectedItem;
                selectedClass.LoadStudentsFromDatabase(connection);
                foreach (Student student in selectedClass.Students)
                {
                    lbStudents.Items.Add(student);
                }
            }
        }

        private void ShowLog()
        {
            // 查询并显示日志
            List<LogEntry> logEntries = log.GetLogEntries();
            lbLog.Items.Clear();
            foreach (LogEntry entry in logEntries)
            {
                lbLog.Items.Add(entry);
            }
        }

        private void btnShowLog_Click(object sender, EventArgs e)
        {
            ShowLog(); // 添加一个按钮或菜单项来触发显示日志操作
        }

        private void btnClearLog_Click(object sender, EventArgs e)
        {
            // 清除日志
            log.ClearLog();
            lbLog.Items.Clear(); // 清空显示的日志列表
        }


        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            connection.Close();
            base.OnFormClosing(e);
        }

    }
}



