﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagementSystem
{
    public class Class
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Student> Students { get; set; }

        public Class(int id, string name)
        {
            Id = id;
            Name = name;
            Students = new List<Student>();
        }

        public Class(string name)
        {
            Name = name;
            Students = new List<Student>();
        }

        public override string ToString()
        {
            return Name;
        }

        public void AddStudent(Student student)
        {
            Students.Add(student);
        }

        // 添加保存班级信息到数据库的方法
        public void SaveToDatabase(SQLiteConnection connection)
        {
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "INSERT INTO Classes (Name) VALUES (@Name)";
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.ExecuteNonQuery();
            }
        }

        // 添加从数据库中删除班级的方法
        public void DeleteFromDatabase(SQLiteConnection connection)
        {
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "DELETE FROM Classes WHERE Id = @Id";
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.ExecuteNonQuery();
            }
        }

        // 添加更新班级信息到数据库的方法
        public void UpdateInDatabase(SQLiteConnection connection)
        {
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "UPDATE Classes SET Name = @Name WHERE Id = @Id";
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.ExecuteNonQuery();
            }
        }

        // 添加从数据库加载学生信息的方法
        public void LoadStudentsFromDatabase(SQLiteConnection connection)
        {
            Students.Clear();
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "SELECT * FROM Students WHERE ClassId = @ClassId";
                cmd.Parameters.AddWithValue("@ClassId", Id);
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int studentId = Convert.ToInt32(reader["Id"]);
                        string studentName = reader["Name"].ToString();
                        string studentGender = reader["Gender"].ToString();
                        int studentAge = Convert.ToInt32(reader["Age"]);
                        string studentDepartment = reader["Department"].ToString();
                        Student student = new Student(studentId, studentName, studentGender, studentAge, studentDepartment, Id); // Pass ClassId
                        Students.Add(student);
                    }
                }
            }
        }
    }
}