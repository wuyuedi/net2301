﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagementSystem
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public string Department { get; set; }
        public int ClassId { get; set; }

        public Student(int id, string name, string gender, int age, string department, int classId)
        {
            Id = id;
            Name = name;
            Gender = gender;
            Age = age;
            Department = department;
            ClassId = classId;
        }

        public Student(string name, string gender, int age, string department, int classId)
        {
            Name = name;
            Gender = gender;
            Age = age;
            Department = department;
            ClassId = classId;
        }

        public override string ToString()
        {
            return Name;
        }

        public void SaveToDatabase(SQLiteConnection connection)
        {
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "INSERT INTO Students (Id, Name, Gender, Age, Department, ClassId) " +
                    "VALUES (@Id, @Name, @Gender, @Age, @Department, @ClassId)";
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@Gender", Gender);
                cmd.Parameters.AddWithValue("@Age", Age);
                cmd.Parameters.AddWithValue("@Department", Department);
                cmd.Parameters.AddWithValue("@ClassId", ClassId);
                cmd.ExecuteNonQuery();
            }
        }

        public void DeleteFromDatabase(SQLiteConnection connection)
        {
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "DELETE FROM Students WHERE Id = @Id";
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateInDatabase(SQLiteConnection connection)
        {
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "UPDATE Students SET Id = @Id, Name = @Name, Gender = @Gender, Age = @Age, Department = @Department, ClassId = @ClassId " +
                    "WHERE Id = @Id";
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@Gender", Gender);
                cmd.Parameters.AddWithValue("@Age", Age);
                cmd.Parameters.AddWithValue("@Department", Department);
                cmd.Parameters.AddWithValue("@ClassId", ClassId);
                cmd.ExecuteNonQuery();
            }
        }
    }
}