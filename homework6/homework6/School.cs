﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace StudentManagementSystem
{
    public class School
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Class> Classes { get; set; }

        public School(int id, string name)
        {
            Id = id;
            Name = name;
            Classes = new List<Class>();
        }

        public School(string name)
        {
            Name = name;
            Classes = new List<Class>();
        }

        public override string ToString()
        {
            return Name;
        }

        public void AddClass(Class newClass)
        {
            Classes.Add(newClass);
        }

        // 添加保存学校信息到数据库的方法
        public void SaveToDatabase(SQLiteConnection connection)
        {
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "INSERT INTO Schools (Name) VALUES (@Name)";
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.ExecuteNonQuery();
            }
        }

        // 添加从数据库中删除学校的方法
        public void DeleteFromDatabase(SQLiteConnection connection)
        {
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "DELETE FROM Schools WHERE Id = @Id";
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.ExecuteNonQuery();
            }
        }

        // 添加更新学校信息到数据库的方法
        public void UpdateInDatabase(SQLiteConnection connection)
        {
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "UPDATE Schools SET Name = @Name WHERE Id = @Id";
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.ExecuteNonQuery();
            }
        }

        // 添加从数据库加载班级信息的方法
        public void LoadClassesFromDatabase(SQLiteConnection connection)
        {
            Classes.Clear();
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "SELECT * FROM Classes WHERE SchoolId = @SchoolId";
                cmd.Parameters.AddWithValue("@SchoolId", Id);
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int classId = Convert.ToInt32(reader["Id"]);
                        string className = reader["Name"].ToString();
                        Class classObj = new Class(classId, className);
                        Classes.Add(classObj);
                    }
                }
            }
        }
    }
}
