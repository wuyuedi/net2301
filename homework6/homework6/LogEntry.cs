﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagementSystem
{
    public class LogEntry
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Action { get; set; }

        public LogEntry(int id, DateTime timestamp, string action)
        {
            Id = id;
            Timestamp = timestamp;
            Action = action;
        }
    }
}

