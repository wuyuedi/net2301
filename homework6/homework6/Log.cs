﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagementSystem
{
    public class Log
    {
        private SQLiteConnection connection;

        public Log(SQLiteConnection connection)
        {
            this.connection = connection;
            CreateTable();
        }

        // 创建Log表
        private void CreateTable()
        {
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "CREATE TABLE IF NOT EXISTS Log (Id INTEGER PRIMARY KEY, Timestamp DATETIME, Action TEXT)";
                cmd.ExecuteNonQuery();
            }
        }

        // 记录日志
        public void LogAction(string action)
        {
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "INSERT INTO Log (Timestamp, Action) VALUES (@Timestamp, @Action)";
                cmd.Parameters.AddWithValue("@Timestamp", DateTime.Now);
                cmd.Parameters.AddWithValue("@Action", action);
                cmd.ExecuteNonQuery();
            }
        }

        // 查询日志
        public List<LogEntry> GetLogEntries()
        {
            List<LogEntry> logEntries = new List<LogEntry>();

            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "SELECT * FROM Log";
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = Convert.ToInt32(reader["Id"]);
                        DateTime timestamp = Convert.ToDateTime(reader["Timestamp"]);
                        string action = reader["Action"].ToString();
                        logEntries.Add(new LogEntry(id, timestamp, action));
                    }
                }
            }

            return logEntries;
        }

        // 清除日志
        public void ClearLog()
        {
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "DELETE FROM Log";
                cmd.ExecuteNonQuery();
            }
        }
    }
}
