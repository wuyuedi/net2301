﻿namespace StudentManagementSystem
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.lbSchools = new System.Windows.Forms.ListBox();
            this.lbClasses = new System.Windows.Forms.ListBox();
            this.lbStudents = new System.Windows.Forms.ListBox();
            this.txtSchoolName = new System.Windows.Forms.TextBox();
            this.btnCreateSchool = new System.Windows.Forms.Button();
            this.btnDeleteSchool = new System.Windows.Forms.Button();
            this.btnUpdateSchool = new System.Windows.Forms.Button();
            this.txtClassName = new System.Windows.Forms.TextBox();
            this.btnCreateClass = new System.Windows.Forms.Button();
            this.btnDeleteClass = new System.Windows.Forms.Button();
            this.btnUpdateClass = new System.Windows.Forms.Button();
            this.txtStudentName = new System.Windows.Forms.TextBox();
            this.txtStudentGender = new System.Windows.Forms.TextBox();
            this.txtStudentAge = new System.Windows.Forms.TextBox();
            this.txtStudentDepartment = new System.Windows.Forms.TextBox();
            this.btnCreateStudent = new System.Windows.Forms.Button();
            this.btnUpdateStudent = new System.Windows.Forms.Button();
            this.txtSearchSchool = new System.Windows.Forms.TextBox();
            this.btnSearchSchool = new System.Windows.Forms.Button();
            this.txtSearchClass = new System.Windows.Forms.TextBox();
            this.btnSearchClass = new System.Windows.Forms.Button();
            this.txtSearchStudent = new System.Windows.Forms.TextBox();
            this.btnSearchStudent = new System.Windows.Forms.Button();
            this.lbLog = new System.Windows.Forms.ListBox();
            this.btnShowLog = new System.Windows.Forms.Button();
            this.btnClearLog = new System.Windows.Forms.Button();
            this.labelSchoolName = new System.Windows.Forms.Label();
            this.labelClassName = new System.Windows.Forms.Label();
            this.labelStudentName = new System.Windows.Forms.Label();
            this.labelStudentGender = new System.Windows.Forms.Label();
            this.labelStudentAge = new System.Windows.Forms.Label();
            this.labelStudentDepartment = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbSchools
            // 
            this.lbSchools.FormattingEnabled = true;
            this.lbSchools.ItemHeight = 18;
            this.lbSchools.Location = new System.Drawing.Point(18, 20);
            this.lbSchools.Margin = new System.Windows.Forms.Padding(4);
            this.lbSchools.Name = "lbSchools";
            this.lbSchools.Size = new System.Drawing.Size(178, 76);
            this.lbSchools.TabIndex = 0;
            this.lbSchools.SelectedIndexChanged += new System.EventHandler(this.lbSchools_SelectedIndexChanged);
            // 
            // lbClasses
            // 
            this.lbClasses.FormattingEnabled = true;
            this.lbClasses.ItemHeight = 18;
            this.lbClasses.Location = new System.Drawing.Point(18, 129);
            this.lbClasses.Margin = new System.Windows.Forms.Padding(4);
            this.lbClasses.Name = "lbClasses";
            this.lbClasses.Size = new System.Drawing.Size(178, 76);
            this.lbClasses.TabIndex = 1;
            this.lbClasses.SelectedIndexChanged += new System.EventHandler(this.lbClasses_SelectedIndexChanged);
            // 
            // lbStudents
            // 
            this.lbStudents.FormattingEnabled = true;
            this.lbStudents.ItemHeight = 18;
            this.lbStudents.Location = new System.Drawing.Point(18, 238);
            this.lbStudents.Margin = new System.Windows.Forms.Padding(4);
            this.lbStudents.Name = "lbStudents";
            this.lbStudents.Size = new System.Drawing.Size(178, 202);
            this.lbStudents.TabIndex = 2;
            // 
            // txtSchoolName
            // 
            this.txtSchoolName.Location = new System.Drawing.Point(268, 23);
            this.txtSchoolName.Margin = new System.Windows.Forms.Padding(4);
            this.txtSchoolName.Name = "txtSchoolName";
            this.txtSchoolName.Size = new System.Drawing.Size(148, 28);
            this.txtSchoolName.TabIndex = 3;
            // 
            // btnCreateSchool
            // 
            this.btnCreateSchool.Location = new System.Drawing.Point(424, 23);
            this.btnCreateSchool.Margin = new System.Windows.Forms.Padding(4);
            this.btnCreateSchool.Name = "btnCreateSchool";
            this.btnCreateSchool.Size = new System.Drawing.Size(112, 34);
            this.btnCreateSchool.TabIndex = 4;
            this.btnCreateSchool.Text = "创建学校";
            this.btnCreateSchool.UseVisualStyleBackColor = true;
            this.btnCreateSchool.Click += new System.EventHandler(this.btnCreateSchool_Click);
            // 
            // btnDeleteSchool
            // 
            this.btnDeleteSchool.Location = new System.Drawing.Point(544, 68);
            this.btnDeleteSchool.Margin = new System.Windows.Forms.Padding(4);
            this.btnDeleteSchool.Name = "btnDeleteSchool";
            this.btnDeleteSchool.Size = new System.Drawing.Size(112, 34);
            this.btnDeleteSchool.TabIndex = 5;
            this.btnDeleteSchool.Text = "删除学校";
            this.btnDeleteSchool.UseVisualStyleBackColor = true;
            this.btnDeleteSchool.Click += new System.EventHandler(this.btnDeleteSchool_Click);
            // 
            // btnUpdateSchool
            // 
            this.btnUpdateSchool.Location = new System.Drawing.Point(544, 23);
            this.btnUpdateSchool.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpdateSchool.Name = "btnUpdateSchool";
            this.btnUpdateSchool.Size = new System.Drawing.Size(112, 34);
            this.btnUpdateSchool.TabIndex = 6;
            this.btnUpdateSchool.Text = "更新学校";
            this.btnUpdateSchool.UseVisualStyleBackColor = true;
            this.btnUpdateSchool.Click += new System.EventHandler(this.btnUpdateSchool_Click);
            // 
            // txtClassName
            // 
            this.txtClassName.Location = new System.Drawing.Point(268, 126);
            this.txtClassName.Margin = new System.Windows.Forms.Padding(4);
            this.txtClassName.Name = "txtClassName";
            this.txtClassName.Size = new System.Drawing.Size(148, 28);
            this.txtClassName.TabIndex = 7;
            // 
            // btnCreateClass
            // 
            this.btnCreateClass.Location = new System.Drawing.Point(424, 121);
            this.btnCreateClass.Margin = new System.Windows.Forms.Padding(4);
            this.btnCreateClass.Name = "btnCreateClass";
            this.btnCreateClass.Size = new System.Drawing.Size(112, 34);
            this.btnCreateClass.TabIndex = 8;
            this.btnCreateClass.Text = "创建班级";
            this.btnCreateClass.UseVisualStyleBackColor = true;
            this.btnCreateClass.Click += new System.EventHandler(this.btnCreateClass_Click);
            // 
            // btnDeleteClass
            // 
            this.btnDeleteClass.Location = new System.Drawing.Point(544, 162);
            this.btnDeleteClass.Margin = new System.Windows.Forms.Padding(4);
            this.btnDeleteClass.Name = "btnDeleteClass";
            this.btnDeleteClass.Size = new System.Drawing.Size(112, 34);
            this.btnDeleteClass.TabIndex = 9;
            this.btnDeleteClass.Text = "删除班级";
            this.btnDeleteClass.UseVisualStyleBackColor = true;
            this.btnDeleteClass.Click += new System.EventHandler(this.btnDeleteClass_Click);
            // 
            // btnUpdateClass
            // 
            this.btnUpdateClass.Location = new System.Drawing.Point(544, 120);
            this.btnUpdateClass.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpdateClass.Name = "btnUpdateClass";
            this.btnUpdateClass.Size = new System.Drawing.Size(112, 34);
            this.btnUpdateClass.TabIndex = 10;
            this.btnUpdateClass.Text = "更新班级";
            this.btnUpdateClass.UseVisualStyleBackColor = true;
            this.btnUpdateClass.Click += new System.EventHandler(this.btnUpdateClass_Click);
            // 
            // txtStudentName
            // 
            this.txtStudentName.Location = new System.Drawing.Point(268, 241);
            this.txtStudentName.Margin = new System.Windows.Forms.Padding(4);
            this.txtStudentName.Name = "txtStudentName";
            this.txtStudentName.Size = new System.Drawing.Size(148, 28);
            this.txtStudentName.TabIndex = 11;
            // 
            // txtStudentGender
            // 
            this.txtStudentGender.Location = new System.Drawing.Point(267, 286);
            this.txtStudentGender.Margin = new System.Windows.Forms.Padding(4);
            this.txtStudentGender.Name = "txtStudentGender";
            this.txtStudentGender.Size = new System.Drawing.Size(148, 28);
            this.txtStudentGender.TabIndex = 27;
            // 
            // txtStudentAge
            // 
            this.txtStudentAge.Location = new System.Drawing.Point(267, 322);
            this.txtStudentAge.Margin = new System.Windows.Forms.Padding(4);
            this.txtStudentAge.Name = "txtStudentAge";
            this.txtStudentAge.Size = new System.Drawing.Size(148, 28);
            this.txtStudentAge.TabIndex = 13;
            // 
            // txtStudentDepartment
            // 
            this.txtStudentDepartment.Location = new System.Drawing.Point(267, 362);
            this.txtStudentDepartment.Margin = new System.Windows.Forms.Padding(4);
            this.txtStudentDepartment.Name = "txtStudentDepartment";
            this.txtStudentDepartment.Size = new System.Drawing.Size(148, 28);
            this.txtStudentDepartment.TabIndex = 14;
            // 
            // btnCreateStudent
            // 
            this.btnCreateStudent.Location = new System.Drawing.Point(424, 238);
            this.btnCreateStudent.Margin = new System.Windows.Forms.Padding(4);
            this.btnCreateStudent.Name = "btnCreateStudent";
            this.btnCreateStudent.Size = new System.Drawing.Size(112, 34);
            this.btnCreateStudent.TabIndex = 15;
            this.btnCreateStudent.Text = "创建学生";
            this.btnCreateStudent.UseVisualStyleBackColor = true;
            this.btnCreateStudent.Click += new System.EventHandler(this.btnCreateStudent_Click);
            // 
            // btnUpdateStudent
            // 
            this.btnUpdateStudent.Location = new System.Drawing.Point(544, 238);
            this.btnUpdateStudent.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpdateStudent.Name = "btnUpdateStudent";
            this.btnUpdateStudent.Size = new System.Drawing.Size(112, 34);
            this.btnUpdateStudent.TabIndex = 17;
            this.btnUpdateStudent.Text = "更新学生";
            this.btnUpdateStudent.UseVisualStyleBackColor = true;
            this.btnUpdateStudent.Click += new System.EventHandler(this.btnUpdateStudent_Click);
            // 
            // txtSearchSchool
            // 
            this.txtSearchSchool.Location = new System.Drawing.Point(268, 68);
            this.txtSearchSchool.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearchSchool.Name = "txtSearchSchool";
            this.txtSearchSchool.Size = new System.Drawing.Size(148, 28);
            this.txtSearchSchool.TabIndex = 18;
            // 
            // btnSearchSchool
            // 
            this.btnSearchSchool.Location = new System.Drawing.Point(424, 68);
            this.btnSearchSchool.Margin = new System.Windows.Forms.Padding(4);
            this.btnSearchSchool.Name = "btnSearchSchool";
            this.btnSearchSchool.Size = new System.Drawing.Size(112, 34);
            this.btnSearchSchool.TabIndex = 19;
            this.btnSearchSchool.Text = "搜索学校";
            this.btnSearchSchool.UseVisualStyleBackColor = true;
            this.btnSearchSchool.Click += new System.EventHandler(this.btnSearchSchool_Click);
            // 
            // txtSearchClass
            // 
            this.txtSearchClass.Location = new System.Drawing.Point(268, 167);
            this.txtSearchClass.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearchClass.Name = "txtSearchClass";
            this.txtSearchClass.Size = new System.Drawing.Size(148, 28);
            this.txtSearchClass.TabIndex = 20;
            // 
            // btnSearchClass
            // 
            this.btnSearchClass.Location = new System.Drawing.Point(424, 163);
            this.btnSearchClass.Margin = new System.Windows.Forms.Padding(4);
            this.btnSearchClass.Name = "btnSearchClass";
            this.btnSearchClass.Size = new System.Drawing.Size(112, 34);
            this.btnSearchClass.TabIndex = 21;
            this.btnSearchClass.Text = "搜索班级";
            this.btnSearchClass.UseVisualStyleBackColor = true;
            this.btnSearchClass.Click += new System.EventHandler(this.btnSearchClass_Click);
            // 
            // txtSearchStudent
            // 
            this.txtSearchStudent.Location = new System.Drawing.Point(268, 407);
            this.txtSearchStudent.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearchStudent.Name = "txtSearchStudent";
            this.txtSearchStudent.Size = new System.Drawing.Size(148, 28);
            this.txtSearchStudent.TabIndex = 22;
            // 
            // btnSearchStudent
            // 
            this.btnSearchStudent.Location = new System.Drawing.Point(424, 405);
            this.btnSearchStudent.Margin = new System.Windows.Forms.Padding(4);
            this.btnSearchStudent.Name = "btnSearchStudent";
            this.btnSearchStudent.Size = new System.Drawing.Size(112, 34);
            this.btnSearchStudent.TabIndex = 23;
            this.btnSearchStudent.Text = "搜索学生";
            this.btnSearchStudent.UseVisualStyleBackColor = true;
            this.btnSearchStudent.Click += new System.EventHandler(this.btnSearchStudent_Click);
            // 
            // lbLog
            // 
            this.lbLog.FormattingEnabled = true;
            this.lbLog.ItemHeight = 18;
            this.lbLog.Location = new System.Drawing.Point(714, 22);
            this.lbLog.Margin = new System.Windows.Forms.Padding(4);
            this.lbLog.Name = "lbLog";
            this.lbLog.Size = new System.Drawing.Size(292, 364);
            this.lbLog.TabIndex = 24;
            // 
            // btnShowLog
            // 
            this.btnShowLog.Location = new System.Drawing.Point(748, 402);
            this.btnShowLog.Margin = new System.Windows.Forms.Padding(4);
            this.btnShowLog.Name = "btnShowLog";
            this.btnShowLog.Size = new System.Drawing.Size(112, 34);
            this.btnShowLog.TabIndex = 25;
            this.btnShowLog.Text = "显示日志";
            this.btnShowLog.UseVisualStyleBackColor = true;
            this.btnShowLog.Click += new System.EventHandler(this.btnShowLog_Click);
            // 
            // btnClearLog
            // 
            this.btnClearLog.Location = new System.Drawing.Point(868, 402);
            this.btnClearLog.Margin = new System.Windows.Forms.Padding(4);
            this.btnClearLog.Name = "btnClearLog";
            this.btnClearLog.Size = new System.Drawing.Size(112, 34);
            this.btnClearLog.TabIndex = 26;
            this.btnClearLog.Text = "清除日志";
            this.btnClearLog.UseVisualStyleBackColor = true;
            this.btnClearLog.Click += new System.EventHandler(this.btnClearLog_Click);
            // 
            // labelSchoolName
            // 
            this.labelSchoolName.AutoSize = true;
            this.labelSchoolName.Location = new System.Drawing.Point(208, 25);
            this.labelSchoolName.Name = "labelSchoolName";
            this.labelSchoolName.Size = new System.Drawing.Size(53, 18);
            this.labelSchoolName.TabIndex = 28;
            this.labelSchoolName.Text = "校名:";
            // 
            // labelClassName
            // 
            this.labelClassName.AutoSize = true;
            this.labelClassName.Location = new System.Drawing.Point(205, 129);
            this.labelClassName.Name = "labelClassName";
            this.labelClassName.Size = new System.Drawing.Size(53, 18);
            this.labelClassName.TabIndex = 29;
            this.labelClassName.Text = "班级:";
            // 
            // labelStudentName
            // 
            this.labelStudentName.AutoSize = true;
            this.labelStudentName.Location = new System.Drawing.Point(206, 244);
            this.labelStudentName.Name = "labelStudentName";
            this.labelStudentName.Size = new System.Drawing.Size(53, 18);
            this.labelStudentName.TabIndex = 30;
            this.labelStudentName.Text = "姓名:";
            // 
            // labelStudentGender
            // 
            this.labelStudentGender.AutoSize = true;
            this.labelStudentGender.Location = new System.Drawing.Point(207, 286);
            this.labelStudentGender.Name = "labelStudentGender";
            this.labelStudentGender.Size = new System.Drawing.Size(53, 18);
            this.labelStudentGender.TabIndex = 31;
            this.labelStudentGender.Text = "性别:";
            // 
            // labelStudentAge
            // 
            this.labelStudentAge.AutoSize = true;
            this.labelStudentAge.Location = new System.Drawing.Point(208, 325);
            this.labelStudentAge.Name = "labelStudentAge";
            this.labelStudentAge.Size = new System.Drawing.Size(53, 18);
            this.labelStudentAge.TabIndex = 32;
            this.labelStudentAge.Text = "年龄:";
            // 
            // labelStudentDepartment
            // 
            this.labelStudentDepartment.AutoSize = true;
            this.labelStudentDepartment.Location = new System.Drawing.Point(208, 365);
            this.labelStudentDepartment.Name = "labelStudentDepartment";
            this.labelStudentDepartment.Size = new System.Drawing.Size(53, 18);
            this.labelStudentDepartment.TabIndex = 33;
            this.labelStudentDepartment.Text = "院系:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(544, 405);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 34);
            this.button1.TabIndex = 16;
            this.button1.Text = "删除学生";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnDeleteStudent_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 472);
            this.Controls.Add(this.btnClearLog);
            this.Controls.Add(this.btnShowLog);
            this.Controls.Add(this.lbLog);
            this.Controls.Add(this.btnSearchStudent);
            this.Controls.Add(this.txtSearchStudent);
            this.Controls.Add(this.btnSearchClass);
            this.Controls.Add(this.txtSearchClass);
            this.Controls.Add(this.btnSearchSchool);
            this.Controls.Add(this.txtSearchSchool);
            this.Controls.Add(this.btnUpdateStudent);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnCreateStudent);
            this.Controls.Add(this.txtStudentName);
            this.Controls.Add(this.txtStudentGender);
            this.Controls.Add(this.txtStudentAge);
            this.Controls.Add(this.txtStudentDepartment);
            this.Controls.Add(this.btnUpdateClass);
            this.Controls.Add(this.btnDeleteClass);
            this.Controls.Add(this.btnCreateClass);
            this.Controls.Add(this.txtClassName);
            this.Controls.Add(this.btnUpdateSchool);
            this.Controls.Add(this.btnDeleteSchool);
            this.Controls.Add(this.btnCreateSchool);
            this.Controls.Add(this.txtSchoolName);
            this.Controls.Add(this.lbStudents);
            this.Controls.Add(this.lbClasses);
            this.Controls.Add(this.lbSchools);
            this.Controls.Add(this.labelSchoolName);
            this.Controls.Add(this.labelClassName);
            this.Controls.Add(this.labelStudentName);
            this.Controls.Add(this.labelStudentGender);
            this.Controls.Add(this.labelStudentAge);
            this.Controls.Add(this.labelStudentDepartment);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "学生管理系统";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbSchools;
        private System.Windows.Forms.ListBox lbClasses;
        private System.Windows.Forms.ListBox lbStudents;
        private System.Windows.Forms.TextBox txtSchoolName;
        private System.Windows.Forms.Button btnCreateSchool;
        private System.Windows.Forms.Button btnDeleteSchool;
        private System.Windows.Forms.Button btnUpdateSchool;
        private System.Windows.Forms.TextBox txtClassName;
        private System.Windows.Forms.Button btnCreateClass;
        private System.Windows.Forms.Button btnDeleteClass;
        private System.Windows.Forms.Button btnUpdateClass;
        private System.Windows.Forms.TextBox txtStudentName;
        private System.Windows.Forms.TextBox txtStudentGender;
        private System.Windows.Forms.TextBox txtStudentAge;
        private System.Windows.Forms.TextBox txtStudentDepartment;
        private System.Windows.Forms.Button btnCreateStudent;
        private System.Windows.Forms.Button btnUpdateStudent;
        private System.Windows.Forms.TextBox txtSearchSchool;
        private System.Windows.Forms.Button btnSearchSchool;
        private System.Windows.Forms.TextBox txtSearchClass;
        private System.Windows.Forms.Button btnSearchClass;
        private System.Windows.Forms.TextBox txtSearchStudent;
        private System.Windows.Forms.Button btnSearchStudent;
        private System.Windows.Forms.ListBox lbLog;
        private System.Windows.Forms.Button btnShowLog;
        private System.Windows.Forms.Button btnClearLog;
        private System.Windows.Forms.Label labelSchoolName;
        private System.Windows.Forms.Label labelClassName;
        private System.Windows.Forms.Label labelStudentName;
        private System.Windows.Forms.Label labelStudentGender;
        private System.Windows.Forms.Label labelStudentAge;
        private System.Windows.Forms.Label labelStudentDepartment;
        private System.Windows.Forms.Button button1;
    }
}
