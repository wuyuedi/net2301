﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace homework4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            // new a node
            TreeNode rootNode1 = new TreeNode("F:\\");
            treeView.Nodes.Add(rootNode1);

            //new a file and its downNode
            DirectoryInfo directoryInfo = new DirectoryInfo("F:\\");
            foreach (DirectoryInfo d in directoryInfo.GetDirectories())
            {
                TreeNode downNode = new TreeNode(d.Name);
                rootNode1.Nodes.Add(downNode);
            }
        }
       

        //
        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //name to judge file or folder
            //fullname include:Folder: or File:
            string fullname = listBox.SelectedItem.ToString();
            string subname = fullname.Substring(fullname.IndexOf(':') + 1);
            //: next =>filename
            string FilePath = Path.Combine(treeView.SelectedNode.FullPath, subname);
            
            if (fullname.StartsWith("Folder:"))
            {
                //no operation include
                treeView.SelectedNode = treeView.SelectedNode.Nodes[subname];
            }
            else if (fullname.StartsWith("File:"))
            {
                string Extension = Path.GetExtension(FilePath);
                //when the file = xxx.exe
                if (Extension.Equals(".exe", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        System.Diagnostics.Process.Start(FilePath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Failed to open the exe.file: {ex.Message}", "Error");
                    }
                }//when the file = xxx.txt
                else if (Extension.Equals(".txt", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        System.Diagnostics.Process.Start("notepad.exe", FilePath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Failed to open the txt.file: {ex.Message}", "Error");
                    }
                }
            }
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //clear the listbox
            listBox.Items.Clear();

            //new a downfile
            string Path = e.Node.FullPath;
            DirectoryInfo directoryInfo = new DirectoryInfo(Path);

            //folder_show
            foreach (DirectoryInfo d in directoryInfo.GetDirectories())
            {
                listBox.Items.Add($"Folder:{d.Name}");
            }

            //file_show
            foreach (FileInfo f in directoryInfo.GetFiles())
            {
                listBox.Items.Add($"File:{f.Name}");
            }
        }
    }
}
