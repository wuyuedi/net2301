﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace homework1b
{
    class Program
    {
        static int totalQuestions = 5;
        static int timeLimitInSeconds = 30; 
        static int currentQuestionNumber = 1;
        static int correctAnswers = 0;
        static Timer timer;
        static Random random = new Random();

        static void Main(string[] args)
        {
            Console.WriteLine("欢迎来到测验!");
            Console.WriteLine($"你有 {timeLimitInSeconds} 秒时间回答 {totalQuestions} 个问题。\n");

            timer = new Timer(1000);
            timer.Elapsed += TimerElapsed;
            timer.Start();

            AskQuestion();

            Console.ReadLine();
        }

        static void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            if (timeLimitInSeconds > 0)
            {
                timeLimitInSeconds--;
                Console.WriteLine($"剩余时间: {timeLimitInSeconds} 秒");
            }
            else
            {
                timer.Stop();
                Console.WriteLine("\n时间到了！测验结束。");
                CalculateScore();
            }
        }

        static void AskQuestion()
        {
            if (currentQuestionNumber <= totalQuestions)
            {
                int num1 = random.Next(1, 11);
                int num2 = random.Next(1, 11);
                char operation = random.Next(0, 2) == 0 ? '+' : '-';

                Console.Write($"问题 {currentQuestionNumber}: {num1} {operation} {num2} = ");
                string userAnswer = Console.ReadLine();

                int correctAnswer = operation == '+' ? num1 + num2 : num1 - num2;
                int userAnswerInt;

                if (int.TryParse(userAnswer, out userAnswerInt))
                {
                    if (userAnswerInt == correctAnswer)
                    {
                        Console.WriteLine("回答正确!\n");
                        correctAnswers++;
                    }
                    else
                    {
                        Console.WriteLine("回答错误.\n");
                    }
                }
                else
                {
                    Console.WriteLine("无效的输入.\n");
                }

                currentQuestionNumber++;
                AskQuestion();
            }
            else
            {
                timer.Stop();
                Console.WriteLine("\n测验结束。");
                CalculateScore();
            }
        }

        static void CalculateScore()
        {
            int score = (correctAnswers * 100) / totalQuestions;
            Console.WriteLine($"你的得分: {score}%");
        }
    }
}
