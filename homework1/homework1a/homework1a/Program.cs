﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace homework1a
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个正整数：");
            if (int.TryParse(Console.ReadLine(), out int x)&& x>0)
            {
                Console.WriteLine("该数字的素数因子为：");
                FindPrimeFactors(x);
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("请输入有效的正整数。");
            }
        }
        static void FindPrimeFactors(int x)
        {
            int d = 2;
            while (x > 1)
            {
                if (x % d == 0)
                {
                    Console.WriteLine(d);
                    x /= d;
                }
                else
                {
                    d++;
                }
            }
        }
    }
}