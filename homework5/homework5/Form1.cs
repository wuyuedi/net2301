﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace homework5
{
    public partial class Form1 : Form
    {
        CancellationTokenSource cancellationTokenSource;
        Dictionary<string, List<string>> PhoneUrlDict = new Dictionary<string, List<string>>();
        Regex phonenumRegex = new Regex(@"\b(0\d{2,3}-)?\d{7,8}\b");
        HttpClient httpClient = new HttpClient();

        public Form1()
        {
            InitializeComponent();
            cancellationTokenSource = new CancellationTokenSource();
        }

        private void stopbutton_Click_1(object sender, EventArgs e)
        {
            cancellationTokenSource.Cancel();
            label.Text = "cancel search";
        }

        
        private async void search_button_Click(object sender, EventArgs e)
        {
            string keyword = txtKeyword.Text;
            clearitem();
            if (keyword == "")
            {
                label.Text = "please input keyword";
            }
            else
            {
                label.Text = "success!";
                List<string> urls = await GetUrls(keyword, cancellationTokenSource.Token);

                foreach (var url in urls)
                {
                    await FindPhoneNum(url, cancellationTokenSource.Token);
                }

                foreach (var phoneurl in PhoneUrlDict)
                {
                    string phone = phoneurl.Key;
                    List<string> Urls = phoneurl.Value;
                    
                    txtPhoneResult.Items.Add(phone + "\r\n");
                    foreach (string url in Urls)
                    {
                        txtUrlResult.Items.Add((url + "\r\n"));
                    }

                    txtUrlResult.Items.Add(("下一个phonenum对应的URL：\n"));
                }
            }
        }

        async Task FindPhoneNum(string url, CancellationToken cancellationToken)
        {
            try
            {
                var response = await httpClient.GetAsync(url, cancellationToken);
                string content = await response.Content.ReadAsStringAsync();
                var matches = phonenumRegex.Matches(content);
                foreach (Match match in matches)
                {
                    string phone = match.Value;
                    if (PhoneUrlDict.ContainsKey(phone))
                    {
                        PhoneUrlDict[phone].Add(url);
                    }
                    else
                    {
                        PhoneUrlDict.Add(phone, new List<string> { url });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        async Task<List<string>> GetUrls(string keyword, CancellationToken cancellationToken)
        {
            List<string> urls = new List<string>();
            string baiduUrl = string.Format("https://www.baidu.com/s?wd={0}", keyword.Replace(" ", "+"));
            string bingUrl = string.Format("https://www.bing.com/search?q={0}", keyword.Replace(" ", "+"));
            Regex urlRegex = new Regex(@"https?://\S+");

            //baidu
            if (baiduButton1.Checked) {
                var tasks = new List<Task<HttpResponseMessage>>() {httpClient.GetAsync(baiduUrl, cancellationToken)};
                var responses = await Task.WhenAll(tasks);
                foreach (var response in responses)
                {
                    string page = await response.Content.ReadAsStringAsync();
                    var includepage = urlRegex.Matches(page);
                    foreach (Match match in includepage)
                    {
                        string url = match.Value;
                        if (urls.Count >= 100)
                        {
                            break;
                        }
                        if (!urls.Contains(url))
                        {
                            urls.Add(url);
                        }
                    }
                    if (urls.Count >= 100)
                    {
                        break;
                    }
                }
            }

            //bing
            if (bingButton.Checked)
            {
                var tasks = new List<Task<HttpResponseMessage>>() {
                httpClient.GetAsync(bingUrl, cancellationToken)
            };
                var responses = await Task.WhenAll(tasks);

                foreach (var response in responses)
                {
                    string page = await response.Content.ReadAsStringAsync();
                    var includepage = urlRegex.Matches(page);
                    foreach (Match match in includepage)
                    {
                        string url = match.Value;
                        if (urls.Count >= 100)
                        {
                            break;
                        }
                        if (!urls.Contains(url))
                        {
                            urls.Add(url);
                        }
                    }
                    if (urls.Count >= 100)
                    {
                        break;
                    }
                }
            }
            return urls;
        }
        void clearitem() 
        {
            txtUrlResult.Items.Clear();
            txtPhoneResult.Items.Clear();
        }
    }
}



