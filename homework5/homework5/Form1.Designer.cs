﻿namespace homework5
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.search_button = new System.Windows.Forms.Button();
            this.txtKeyword = new System.Windows.Forms.TextBox();
            this.keyword = new System.Windows.Forms.Label();
            this.stopbutton = new System.Windows.Forms.Button();
            this.txtUrlResult = new System.Windows.Forms.ListBox();
            this.txtPhoneResult = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.baiduButton1 = new System.Windows.Forms.RadioButton();
            this.bingButton = new System.Windows.Forms.RadioButton();
            this.label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Fugaz One", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(458, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(256, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "winform crawler";
            // 
            // search_button
            // 
            this.search_button.Location = new System.Drawing.Point(675, 97);
            this.search_button.Name = "search_button";
            this.search_button.Size = new System.Drawing.Size(75, 42);
            this.search_button.TabIndex = 1;
            this.search_button.Text = "search";
            this.search_button.UseVisualStyleBackColor = true;
            this.search_button.Click += new System.EventHandler(this.search_button_Click);
            // 
            // txtKeyword
            // 
            this.txtKeyword.Location = new System.Drawing.Point(383, 106);
            this.txtKeyword.Name = "txtKeyword";
            this.txtKeyword.Size = new System.Drawing.Size(256, 28);
            this.txtKeyword.TabIndex = 2;
            // 
            // keyword
            // 
            this.keyword.AutoSize = true;
            this.keyword.Location = new System.Drawing.Point(288, 111);
            this.keyword.Name = "keyword";
            this.keyword.Size = new System.Drawing.Size(89, 18);
            this.keyword.TabIndex = 3;
            this.keyword.Text = "keyword：";
            // 
            // stopbutton
            // 
            this.stopbutton.Location = new System.Drawing.Point(779, 96);
            this.stopbutton.Name = "stopbutton";
            this.stopbutton.Size = new System.Drawing.Size(75, 43);
            this.stopbutton.TabIndex = 5;
            this.stopbutton.Text = "stop";
            this.stopbutton.UseVisualStyleBackColor = true;
            this.stopbutton.Click += new System.EventHandler(this.stopbutton_Click_1);
            // 
            // txtUrlResult
            // 
            this.txtUrlResult.FormattingEnabled = true;
            this.txtUrlResult.ItemHeight = 18;
            this.txtUrlResult.Location = new System.Drawing.Point(46, 250);
            this.txtUrlResult.Name = "txtUrlResult";
            this.txtUrlResult.Size = new System.Drawing.Size(753, 202);
            this.txtUrlResult.TabIndex = 6;
            // 
            // txtPhoneResult
            // 
            this.txtPhoneResult.FormattingEnabled = true;
            this.txtPhoneResult.ItemHeight = 18;
            this.txtPhoneResult.Location = new System.Drawing.Point(845, 250);
            this.txtPhoneResult.Name = "txtPhoneResult";
            this.txtPhoneResult.Size = new System.Drawing.Size(238, 202);
            this.txtPhoneResult.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Showcard Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(351, 204);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 30);
            this.label2.TabIndex = 8;
            this.label2.Text = "URL";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Showcard Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(881, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(146, 30);
            this.label3.TabIndex = 9;
            this.label3.Text = "phonenum";
            // 
            // baiduButton1
            // 
            this.baiduButton1.AutoSize = true;
            this.baiduButton1.Location = new System.Drawing.Point(424, 166);
            this.baiduButton1.Name = "baiduButton1";
            this.baiduButton1.Size = new System.Drawing.Size(78, 22);
            this.baiduButton1.TabIndex = 10;
            this.baiduButton1.TabStop = true;
            this.baiduButton1.Text = "Baidu";
            this.baiduButton1.UseVisualStyleBackColor = true;
            // 
            // bingButton
            // 
            this.bingButton.AutoSize = true;
            this.bingButton.Location = new System.Drawing.Point(636, 166);
            this.bingButton.Name = "bingButton";
            this.bingButton.Size = new System.Drawing.Size(69, 22);
            this.bingButton.TabIndex = 11;
            this.bingButton.TabStop = true;
            this.bingButton.Text = "Bing";
            this.bingButton.UseVisualStyleBackColor = true;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Shrikhand", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(895, 106);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(0, 26);
            this.label.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1167, 513);
            this.Controls.Add(this.label);
            this.Controls.Add(this.bingButton);
            this.Controls.Add(this.baiduButton1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPhoneResult);
            this.Controls.Add(this.txtUrlResult);
            this.Controls.Add(this.stopbutton);
            this.Controls.Add(this.keyword);
            this.Controls.Add(this.txtKeyword);
            this.Controls.Add(this.search_button);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button search_button;
        private System.Windows.Forms.TextBox txtKeyword;
        private System.Windows.Forms.Label keyword;
        private System.Windows.Forms.Button stopbutton;
        private System.Windows.Forms.ListBox txtUrlResult;
        private System.Windows.Forms.ListBox txtPhoneResult;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton baiduButton1;
        private System.Windows.Forms.RadioButton bingButton;
        private System.Windows.Forms.Label label;
    }
}

