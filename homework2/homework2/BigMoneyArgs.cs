using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace homework2
{
    public class BigMoneyArgs
    {
        public int AccountNumber;
        public float Count;
        public BigMoneyArgs(int accountNumber, float Amount)
        {
            AccountNumber = accountNumber;
            Count = Amount;
        }
    }
}
