using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace homework2
{
     public class Account
    {
        protected int id;
        protected float money;
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public float Money
        {
            get
            {
                return money;
            }
            set
            {
                money = value;
            }
        }
        public Account(int accountNumber,float initialBalance)
        {
            this.id = accountNumber;
            this.money = initialBalance;
        }
        public void addMoney(float count)
        {
            if (count > 0)
            {
                money += count;
            }
        }
        virtual public bool subMoney(float count)
        {
            if (count > 0 && money >= count)
            {
                money -= count;
                return true;
            }
            return false;
        }
    }
}
