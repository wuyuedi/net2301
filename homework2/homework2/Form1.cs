using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Rebar;
namespace homework2
{
    public partial class Form1 : Form
    {
        private Bank bank;
        private ATM atm;
        Account account;
        int accountID;
        public Form1()
        {
            InitializeComponent();
            bank = new Bank();
            atm = new ATM(bank);
            atm.BigMoneyFetched += Atm_BigMoneyFetched;
        }
        private void Atm_BigMoneyFetched(object sender, BigMoneyArgs e)
        {
            label8.Text = "警告取出大金额";
        }
        private void account_build_Click(object sender, EventArgs e)
        {
            int accountNumber = int.Parse(textBoxAccount.Text);
            float initMoney = float.Parse(InitMoney.Text);
            float creditLimit = float.Parse(CreditMoney.Text); 
            if (credit.Checked)
            {
                creditAccount creditAccount = new creditAccount(accountNumber, initMoney, creditLimit);
                bank.AddAccount(creditAccount);
                MessageBox.Show("信用账户创建成功");
            }
            else if (others.Checked)
            {
                Account account = new Account(accountNumber, initMoney);
                bank.AddAccount(account);
                MessageBox.Show("普通账户创建成功");
            }
        }
        private void subMoneyButton_Click(object sender, EventArgs e)
        {
            int accountNumber = int.Parse(textBoxAccount.Text);
            float money = float.Parse(Money.Text);
            Account account = bank[accountNumber];
            if (account != null)
            {
                if (account is creditAccount cA)
                {
                    if (cA.subMoney(money))
                    {
                        MessageBox.Show("取款成功！");
                    }
                    else
                    {
                        MessageBox.Show("取款失败，请重新输入金额！");
                    }
                }
                else
                {
                    if (account.subMoney(money))
                    {
                        MessageBox.Show("取款成功！");
                    }
                    else
                    {
                        MessageBox.Show("取款失败，请重新输入金额！");
                    }
                }
                if (money > 10000)
                {
                    atm.SubMoney(accountNumber, money);
                }
            }
            else
            {
                MessageBox.Show("没有找到账户！");
            }
        }
        private void addMoneyButton_Click(object sender, EventArgs e)
        {
            int accountNumber = int.Parse(textBoxAccount.Text);
            float money = float.Parse(Money.Text);
            Account account = bank[accountNumber];
            if (account != null)
            {
                account.addMoney(money);
                MessageBox.Show("存钱成功！");
            }
            else
            {
                MessageBox.Show("没有找到该账户！");
            }
        }
    }
}
