﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework2
{
    class Bank
    {
        private List<Account> accountsList;

        public Bank()
        {
            accountsList = new List<Account>();
        }

        public void AddAccount(Account account)
        {
            accountsList.Add(account);
        }

        public Account this[int accountNumber]
        {
            get
            {
                return accountsList.Find(account => account.Id == accountNumber);
            }
        }
    }
}
