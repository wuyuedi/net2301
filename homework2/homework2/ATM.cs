using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace homework2
{
    internal class ATM
    {
        private Bank bank;
        private Random random;
        public event EventHandler<BigMoneyArgs> BigMoneyFetched; 
        public ATM(Bank bank)
        {
            this.bank = bank;
            this.random = new Random();
        }
        public bool SubMoney(int accountNumber, float count)
        {
            Account account = bank[accountNumber];
            if (account != null)
            {
                if (count > 10000)
                {
                    OnBigMoneyFetched(new BigMoneyArgs(accountNumber, count));
                }
                if (random.Next(100) < 30)
                {
                    throw new BadCashException("有坏钞票");
                }
                return account.subMoney(count);
            }
            return false;
        }
        protected virtual void OnBigMoneyFetched(BigMoneyArgs e)
        {
            BigMoneyFetched?.Invoke(this, e);
        }
    }
}
