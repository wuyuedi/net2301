using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace homework2
{
    public class creditAccount : Account
    {
        private float creditMoney;
        public float CreditMoney
        {
            get
            {
                return creditMoney;
            }
            set
            {
                creditMoney = value;
            }
        }
        public creditAccount(int accountNumber,float initialBalance, float creditMoney)
           : base(accountNumber, initialBalance)
        {
            this.creditMoney = creditMoney;
        }
        override public bool subMoney(float count)
        {
            if (count > 0 && (money + creditMoney) >= count)
            {
                money -= count;
                return true;
            }
            return false;
        }
    }
}
